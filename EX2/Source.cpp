#include <ilcplex/ilocplex.h>
#include <vector>
#include <algorithm>
ILOSTLBEGIN

typedef struct {
	int from, to, id;
} Edge;

int id = 0;
class Graph {
public:
	int nvertices, nedges;
	vector<Edge*> adjacencia;

	Graph(int nvertices) {
		this->nvertices = nvertices;
	}
	Graph(string filename) {
		ifstream file(filename);
		file >> nvertices >> nedges;
		int a, b;
		while (file >> a >> b) {
			a--;
			b--;
			this->addEdge(a, b);
		}
	}
	void addEdge(int from, int to) {
		Edge* e = new Edge();
		e->to = to;
		e->from = from;
		e->id = id++;
		adjacencia.push_back(e);

	}

	vector<Edge*> getEdgesByFirst(int first, Edge* ignore) {
		vector<Edge*> filtered;
		vector<bool> ignoreIndex;
		ignoreIndex.resize(adjacencia.size());
		for (int i = 0; i < adjacencia.size(); i++) {
			Edge* e = adjacencia[i];
			if (e->from == first && !(e->from == ignore->from && e->to == ignore->to)) {
				filtered.push_back(e);
				ignoreIndex[i] = true;
			}
		}
		int i = 0;
		for (Edge* e : adjacencia) {
			if (e->to == first && !ignoreIndex[i] && !(e->from == ignore->from && e->to == ignore->to))
				filtered.push_back(e);
			i++;
		}
		return filtered;
	}
};

int main(int argc, char **argv) {
	IloEnv env;
	try {
		IloModel model(env);
		Graph* g = new Graph("in.txt");
		IloInt v = g->nvertices, e = g->nedges;
		IloArray<IloBoolVarArray> x(env, e);
		for (IloInt i = 0; i < e; i++)
			x[i] = IloBoolVarArray(env, e);
		vector<Edge*> edges = g->adjacencia;
		for (IloInt i = 0; i < e; i++)
			model.add(IloSum(x[i]) == 1);
		for (IloInt i = 0; i < e; i++) {
			Edge* edge = edges[i];
			int from = edge->from, to = edge->to;
			vector<Edge*> fst = g->getEdgesByFirst(from, edge), scd = g->getEdgesByFirst(to, edge);
			for (IloInt c = 0; c < e; c++) {
				for (auto f : fst)
					model.add(x[f->id][c] + x[edge->id][c] <= 1);
				for (auto s : scd)
					model.add(x[s->id][c] + x[edge->id][c] <= 1);
			}
		}
		IloExpr cost(env);
		for (IloInt i = 0; i < e; i++)
			for (IloInt j = 0; j < e; j++)
				cost += (j + 1) * x[i][j];
		model.add(IloMinimize(env, cost));
		IloCplex cplex(model);
		cplex.solve();
		env.out() << "Solution status = " << cplex.getStatus() << endl;
		env.out() << "Solution value = " << cplex.getObjValue() << endl;
	}
	catch (IloException &ex) {
		cerr << "Concert exception caught: " << ex << endl;
	}
	env.end();
	return 0;
}