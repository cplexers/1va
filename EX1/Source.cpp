#include <ilcplex/ilocplex.h>
#include <vector>
#include <algorithm>
#include <set>
ILOSTLBEGIN

int id = 0;

typedef struct {
	int from, to, cost, id;
} Edge;

class Graph {

public:
	int nvertices, nedges, id = 0;
	vector<int> terminals;
	vector<Edge*> adjacencia;

	Graph(int nvertices) {
		this->nvertices = nvertices;
	}
	Graph(string filename) {
		ifstream file(filename);
		file >> nvertices >> nedges;
		int a, b, c, cont = nedges;
		while (cont--) {
			file >> a >> b >> c;
			a--;
			b--;
			this->addEdge(a, b, c);
		}
		int nterminal;
		file >> nterminal;
		while (file >> a)
			terminals.push_back(a - 1);
	}
	void addEdge(int from, int to, int cost) {
		Edge* e = new Edge();
		e->to = to;
		e->cost = cost;
		e->from = from;
		e->id = id++;
		adjacencia.push_back(e);

	}

	vector<Edge*> getEdgesByFirst(int first) {
		vector<Edge*> filtered;
		vector<bool> ignoreIndex;
		ignoreIndex.resize(adjacencia.size());
		for (int i = 0; i < adjacencia.size(); i++) {
			Edge* e = adjacencia[i];
			if (e->from == first) {
				filtered.push_back(e);
				ignoreIndex[i] = true;
			}
		}
		int i = 0;
		for (Edge* e : adjacencia) {
			if (e->to == first && !ignoreIndex[i])
				filtered.push_back(e);
			i++;
		}
		return filtered;
	}
};

int main(int argc, char **argv) {
	IloEnv env;
	try {
		IloModel model(env);
		Graph* g = new Graph("in.txt");
		int e = g->nedges, v = g->nvertices;
		IloBoolVarArray x(env, e), y(env, v);
		IloArray<IloNumVarArray> c(env, e);
		for (IloInt i = 0; i < e; i++)
			c[i] = IloNumVarArray(env, 2, 0, IloInfinity);
		vector<int> terminals = g->terminals;
		for (auto t : terminals) {
			vector<Edge*> edges = g->getEdgesByFirst(t);
			if (edges.size() == 0)
				continue;
			IloExpr sum(env);
			for (IloInt i = 0; i < edges.size(); i++)
				sum += x[edges[i]->id];
			model.add(sum >= 1);
		}
		vector<Edge*> edges = g->adjacencia;
		for (IloInt j = 0; j < e; j++) {
			model.add(x[edges[j]->id] <= y[edges[j]->from]);
			model.add(x[edges[j]->id] <= y[edges[j]->to]);
		}
		model.add(IloSum(y) - IloSum(x) == 1);
		for (IloInt i = 0; i < e; i++)
			model.add(c[edges[i]->id][0] + c[edges[i]->id][1] == 2 * x[edges[i]->id]);
		for (IloInt i = 0; i < v; i++) {
			IloExpr sum(env);
			vector<Edge*> connected = g->getEdgesByFirst(i);
			for (auto conn : connected)
				sum += c[conn->id][conn->from == i ? 0 : 1];
			model.add(sum <= 2.0f - (2.0f / (double)v));
		}
		IloExpr sum(env);
		for (IloInt i = 0; i < e; i++) {
			Edge* edge = edges[i];
			sum += edge->cost * x[edge->id];
		}
		model.add(IloMinimize(env, sum));
		IloCplex cplex(model);
		cplex.solve();
		env.out() << "Solution status = " << cplex.getStatus() << endl;
		env.out() << "Solution value = " << cplex.getObjValue() << endl;
	}
	catch (IloException &ex) {
		cerr << "Concert exception caught: " << ex << endl;
	}
	env.end();
	return 0;
}