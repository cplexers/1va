#include <ilcplex/ilocplex.h>
#include <vector>
#include <set>
ILOSTLBEGIN

class Graph {
private:
	vector<set<int>> adjacencia;

public:
	int nvertices, verticesleft, verticesright;

	Graph(int nvertices) {
		this->nvertices = nvertices;
		adjacencia.resize(nvertices + 1);
	}
	Graph(string filename) {
		ifstream file(filename);
		int verticesleft, verticesright;
		int edges = 0;
		file >> verticesleft >> verticesright >> edges;
		this->nvertices = verticesleft + verticesright;
		this->verticesleft = verticesleft;
		this->verticesright = verticesright;

		adjacencia.resize(nvertices);
		int a, b;
		while (file >> a >> b) {
			b += verticesleft - 1;
			a--;
			this->addEdge(a, b);
		}
	}
	void addEdge(int from, int to) {
		adjacencia[from].insert(to);
		adjacencia[to].insert(from);
	}
	set<int> getNeighboringVertices(int vertex) {
		return adjacencia[vertex];
	}
};

int main(int argc, char **argv) {
	IloEnv env;
	try {
		IloModel model(env);
		Graph* g = new Graph("in.txt");
		IloInt v = g->nvertices;
		IloBoolVarArray x(env, v);
		for (IloInt i = 0; i < g->verticesleft; i++) {
			for (IloInt j = g->verticesleft; j < v; j++) {
				set<int> neigh = g->getNeighboringVertices(i);
				if (!neigh.count(j) && i != j)
					model.add(x[i] + x[j] <= 1);
			}
		}
		IloExpr left(env), right(env);
		for (IloInt i = 0; i < v; i++) {
			if (i < g->verticesleft)
				left += x[i];
			else
				right += x[i];
		}
		model.add(left == right);
		model.add(IloMaximize(env, IloSum(x)));
		IloCplex cplex(model);
		cplex.solve();
		env.out() << "Solution status = " << cplex.getStatus() << endl;
		env.out() << "Solution value = " << cplex.getObjValue() << endl;
	}
	catch (IloException &ex) {
		cerr << "Concert exception caught: " << ex << endl;
	}
	env.end();
	return 0;
}